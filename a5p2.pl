likes(a, killing).
likes(b, c).
likes(c, b).
isDead(d, true).
likes(b, dancers).
likes(e, food).
isDead(a, false).
isDead(b, false).
isDead(c, false).
isDead(e, false).
hates(c, footMassagers).

killer(X) :- likes(X, killing).
married(X, Y) :- \+(X = Y), likes(X, Y), likes(Y, X).
