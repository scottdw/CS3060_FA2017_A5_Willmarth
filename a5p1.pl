%commands to the interpreter are submitted from stdin input ('show input' box below)
%'halt.' will be automatically appended to stdin input.
%swi-prolog 7.2.3

beats(fire, grass).
beats(grass, water).
beats(water, fire).
beats(bug, grass).
beats(fire, bug).
beats(rock, bug).
beats(rock, fire).
beats(grass, rock).
beats(water, rock).
beats(flying, grass).
beats(flying, bug).
beats(rock, flying).
beats(electric, flying).
beats(electric, water).
beats(ground, electric).
beats(ground, rock).
beats(ground, fire).
beats(water, ground).
beats(grass, ground).

notEffective(fire, water).
notEffective(fire, rock).
notEffective(fire, fire).
notEffective(water, water).
notEffective(water, grass).
notEffective(grass, fire).
notEffective(grass, bug).
notEffective(grass, flying).
notEffective(bug, fire).
notEffective(bug, flying).
notEffective(ground, flying).
notEffective(rock, ground).
notEffective(flying, rock).
notEffective(flying, electric).
notEffective(elctric, grass).
notEffective(electric, electric).
notEffective(electric, ground).

type(charmander, fire).
type(squirtle, water).
type(bulbasaur, grass).
type(caterpie, bug).
type(pidgey, flying).
type(geodude, rock).
type(pikachu, electric).
type(onix, ground).

typeAdv(A, B) :- type(A, C), type(B, D), beats(C, D).
typeDis(A, B) :- type(A, C), type(B, D), notEffective(C, D).
weakTo(Pokemon, Attack) :- type(Pokemon, Type), beats(Attack, Type).

%Questions: what type beats what -> beats(fire, Y).
%Questions: what type does a pokemon have -> type(onix, Y).
%Questions: Does a pokemon have a type advantage -> typeAdv(charmander, bulbasaur).
%Questions: Does a pokemon have a type disadvantage -> typeDis(charmander, squirtle).
%Questions: Is a pokemon weak to a type -> weakTo(pidgey, rock).
