loading:
consult('a5p1.pl').
reconsult('a5p1.pl').

beats(fire, Y). -> Y = grass
beats(water, Y). -> Y = fire
type(onix, Type). -> Type = ground
typeAdv(charmander, bulbasaur). -> true
typeAdv(charmander, squirtle). -> false
typeDis(pidgey, pikachu). -> true
typeDis(pidgey, pidgey). -> false
weakTo(caterpie, rock). -> true
weakTo(geodude, fire). -> false


consult('a5p2.pl').
reconsult('a5p2.pl').

married(b,c) -> true
married(d,e) -> false
killer(a) -> true
killer(b) -> false
likes(a, Y) -> Y = killing
likes(e, Y) -> Y = food
hates(c, Y) -> Y = footMassagers

